<?php

/**
 * @file
 * Module file for the course synchronization module.
 */

/**
 * Implements hook_entity_diff()
 */
function course_sync_entity_diff($entity_type, $source, $target) {
  $diff = array();
  if ($entity_type == 'node' && course_node_is_course($source)) {
    // @todo Has the course actually changed?
    $diff[] = 'course';
  }
  return $diff;
}

/**
 * Implements hook_entity_clone_dependencies()
 */
function course_sync_entity_clone_dependencies($entity_type, $source) {
  $dependencies = array();
  
  if ($entity_type == 'node' && course_node_is_course($source)) {
    if (isset($source->course) && !empty($source->course['objects'])) {
      foreach ($source->course['objects'] as $coid => $object) {
        if (!empty($object->instance)) {
          $dependencies[] = array(
            'type' => 'node',
            'id' => $object->instance,
            'description' => "course:coid-{$coid}"
          );
        }
      }
    }
  }
  
  return $dependencies;
}

/**
 * Implements hook_entity_clone_initialize()
 */
function course_sync_entity_clone_initialize($entity_type, $source, $target, $original_target = NULL) {
  if ($entity_type == 'node' && course_node_is_course($target)) {
    $target->course = array(
      'nid' => $target->nid,
      'outline' => $source->course['outline'],
      'credits' => $source->course['credits'],
      'open' => $source->course['open'],
      'close' => $source->course['close'],
      'duration' => $source->course['duration'],
      'external_id' => $source->course['external_id'],
      'objects' => array()
    );
  }
}

/**
 * Implements hook_entity_clone_finalize()
 */
function course_sync_entity_clone_finalize($entity_type, $source, $target, $new) {
  if ($entity_type == 'node' && course_node_is_course($target)) {
    $couuids = array();
    
    foreach ($source->course['objects'] as $coid => $object) {
      $node_instance = NULL;
      
      if ($object->instance != 0) {
        $node_instance = ContentSyncEntity::getTargetBySourceId('node', $object->instance);
        if (empty($node_instance)) {
          watchdog('content_sync', 'Course %title cannot be correctly imported: Unable to find master node instance !id. Please ensure the original course is properly configured.', array('%title' => $target->title), WATCHDOG_WARNING);
        }
      }
      
      $node_instance_id = empty($node_instance) ? 0 : $node_instance->nid;
      
      $couuids[] = $object->uuid;
      db_merge('course_outline')
        ->condition('uuid', $object->uuid)
        ->fields(array(
          'nid' => $target->nid,
          'module' => $object->module,
          'title' => $object->title,
          'object_type' => $object->object_type,
          'enabled' => $object->enabled,
          'info' => $object->info,
          'instance' => $node_instance_id,
          'required' => $object->required,
          'weight' => $object->weight,
          'hidden' => $object->hidden,
          'duration' => $object->duration,
          'uuid' => $object->uuid,
          'data' => $object->data,
        ))->execute();
    }
    
    // Prevent the deletion of course parts which have been already fulfilled by any user
    // Foreach course outline item that needs to be deleted get the number of students who fulfilled it
    $fulfilled = db_query(
      'SELECT co.coid, COUNT(cof.coid) FROM {course_outline} co'
        . ' LEFT JOIN {course_outline_fulfillment} cof ON cof.coid = co.coid'
        . ' WHERE co.nid = :nid' . (empty($couuids) ? '' : ' AND co.uuid NOT IN (:uuids)')
        . ' GROUP BY co.coid',
      array(':nid' => $target->nid) + (empty($couuids) ? array() : array(':uuids' => $couuids))
    )->fetchAllKeyed();
    
    // Get a list of course outline items to delete
    $coids = array();
    foreach ($fulfilled as $coid => $i) {
      if ($i > 0) {
        watchdog('content_sync', 'Course %title partially imported: cannot delete course outline item %coid as it has !i users assosiated with', array(
          '%title' => $target->title,
          '!i' => $i,
          '%coid' => $coid
        ), WATCHDOG_WARNING);
      }
      else {
        $coids[] = $coid;
      }
    }

    if (!empty($coids)) {
      db_delete('course_outline')
        ->condition('nid', $target->nid)
        ->condition('coid', $coids, 'IN')
        ->execute();
    }
    
    watchdog('content_sync', '%title course outline has been updated', array('%title' => $source->title), WATCHDOG_INFO);
  }
}
