<?php

/**
 * @file
 * Module file for the node synchronization module.
 */

module_load_include('inc', 'content_sync', 'content_sync.entity');
module_load_include('inc', 'content_sync', 'content_sync.admin');

define('NODE_SYNC_UID_TASK_NO_ACTION', 0);
define('NODE_SYNC_UID_TASK_ADMIN', 1);
define('NODE_SYNC_UID_TASK_LOGGED_USER', 2);
define('NODE_SYNC_UID_TASK_SYNCHRONIZE', 3);

/**
 * Implements hook_menu()
 */
function node_sync_menu() {
  return array(
    'node/%/synchronize' => array(
      'title' => 'Synchronize',
      'description' => 'Synchronize with the master Drupal site',
      'page callback' => 'content_sync_sync_entity_page',
      'page arguments' => array('node', 1),
      'access callback' => 'user_access',
      'access arguments' => array('synchronize content'),
      'file' => 'content_sync.admin.inc',
      'file path' => drupal_get_path('module', 'content_sync'),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE | MENU_CONTEXT_PAGE,
      'weight' => 99,
    )
  );
}

/**
 * Implements hook_form_FORM_ID_alter()
 */
function node_sync_form_content_sync_config_form_alter(&$form, &$form_state) {
  $form['main']['node'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'node_sync_published_task' => array(
      '#title' => t('Publish/unpublish nodes'),
      '#type' => 'checkbox',
      '#default_value' => variable_get('node_sync_published_task', 0)
    ),
    'node_sync_vid_task' => array(
      '#title' => t('Create a new revision when synchronizing'),
      '#type' => 'checkbox',
      '#default_value' => variable_get('node_sync_vid_task', 1)
    ),
    'node_sync_uid_task_creation' => array(
      '#title' => t('When synchronizing a new node'),
      '#type' => 'select',
      '#options' => array(
        NODE_SYNC_UID_TASK_ADMIN => t('Assign it to user 1'),
        NODE_SYNC_UID_TASK_LOGGED_USER => t('Assign it to the logged in user'),
        NODE_SYNC_UID_TASK_SYNCHRONIZE => t('Assign it to the original user (synchronize user entity)'),
      ),
      '#default_value' => variable_get('node_sync_uid_task_creation', NODE_SYNC_UID_TASK_ADMIN),
      '#disabled' => TRUE
    ),
    'node_sync_uid_task_editing' => array(
      '#title' => t('When synchronizing an existing node'),
      '#type' => 'select',
      '#options' => array(
        NODE_SYNC_UID_TASK_NO_ACTION => t('Do never change the owner'),
        NODE_SYNC_UID_TASK_ADMIN => t('Assign it to user 1'),
        NODE_SYNC_UID_TASK_LOGGED_USER => t('Assign it to the logged in user'),
        NODE_SYNC_UID_TASK_SYNCHRONIZE => t('Assign it to the original user (synchronize user entity)'),
      ),
      '#default_value' => variable_get('node_sync_uid_task_editing', NODE_SYNC_UID_TASK_NO_ACTION),
      '#disabled' => TRUE
    ),
  );
}

/**
 * Implements hook_admin_paths()
 */
function node_sync_admin_paths() {
  return array('node/*/synchronize' => TRUE);
}

/**
 * Implements hook_entity_diff()
 */
function node_sync_entity_diff($entity_type, $source, $target) {
  $diff = array();
  if ($entity_type == 'node') {
    // tnode
    if (!empty($source->tnid) || !empty($target->tnid)) {
      if (empty($source->tnid) || empty($target->tnid)) {
        $diff[] = 'tnid';
      }
      else {
        $tnode1 = ContentSyncEntity::getSource('node', $source->tnid);
        $tnode2 = ContentSyncEntity::getOriginalTarget('node', $target->tnid);
        
        $uuid1 = entity_extract_uuid('node', $tnode1);
        $uuid2 = entity_extract_uuid('node', $tnode2);
        
        if ($uuid1 != $uuid2) {
          $diff[] = 'tnid';
        }
      }
    }
    
    // other properties
    $checks = array('title');
    
    if (variable_get('node_sync_published_task', 0)) {
      $checks[] = 'published';
    }
    
    foreach ($checks as $p) {
      if ($source->{$p} != $target->{$p}) {
        $diff[] = $p;
      }
    }
  }
  return $diff;
}

/**
 * Implements hook_entity_clone_class_alter()
 */
function node_sync_entity_clone_class_alter(&$class) {
  $class['node'] = array('class' => 'NodeSync');
}

class NodeSync extends ContentSyncEntityCloneDefault {
  public function load($entityId) {
    return node_load($entityId);
  }

  public function save($entity) {
    return node_save($entity);
  }
  
  public function delete($entityId) {
    return node_delete($entityId);
  }

  public function sourceDependencies($source) {
    $dependencies = array();

    // Tnode
    if (!empty($source->tnid)) {
      $dependencies[] = array(
        'type' => 'node',
        'id' => $source->tnid,
        'description' => 'tnid'
      );
    }
    
    if ($source->uid != 0 && $source->uid != 1) {
      $original = ContentSyncEntity::getOriginalTargetByUuid('node', entity_extract_uuid('node', $source));
      
      $uid_task = empty($original)
        ? variable_get('node_sync_uid_task_creation', NODE_SYNC_UID_TASK_ADMIN)
        : variable_get('node_sync_uid_task_editing', NODE_SYNC_UID_TASK_NO_ACTION);
      
      if ($uid_task == NODE_SYNC_UID_TASK_SYNCHRONIZE) {
        $dependencies[] = array(
          'type' => 'user',
          'id' => $source->uid,
          'description' => 'owner'
        );
      }
    }
    
    // Organic groups
    if (module_exists('og') && !empty($source->og_group_ref)) {
      foreach ($source->og_group_ref as $values) {
        foreach ($values as $value) {
          $group = ContentSyncEntity::getSource('node', $value['target_id']);
          $uuid = entity_extract_uuid('node', $group);
          if (!ContentSyncEntity::getOriginalTargetByUuid('node', $uuid)) {
            watchdog('content_sync', 'Organic group node %title not found. A new node will be added with no users assosiated.', array(
              '%title' => $group->title
            ), WATCHDOG_WARNING);
          }
        }
      }
    }

    return $dependencies;
  }

  public function targetInitialize($source, $originalTarget = NULL) {
    global $user;
    
    $target = parent::targetInitialize($source, $originalTarget);

    // Tnode
    $target->tnid = empty($originalTarget) || empty($originalTarget->tnid) ? NULL : $originalTarget->tnid;

    $uid_task = empty($originalTarget) 
      ? variable_get('node_sync_uid_task_creation', NODE_SYNC_UID_TASK_ADMIN)
      : variable_get('node_sync_uid_task_editing', NODE_SYNC_UID_TASK_NO_ACTION);

    // Existing entity
    switch ($uid_task) {
      case NODE_SYNC_UID_TASK_NO_ACTION:
        $target->uid = $originalTarget->uid;
        break;

      case NODE_SYNC_UID_TASK_LOGGED_USER:
        $target->uid = $user->uid;
        break;

      case NODE_SYNC_UID_TASK_ADMIN:
      case NODE_SYNC_UID_TASK_SYNCHRONIZE:
        $target->uid = 1;
        break;
    }
    
    // Generates a new revision
    if (variable_get('node_sync_vid_task', 1)) {
      $target->revision = 1;
      $target->log = t('Synchronized with the master database on !d', array('!d' => format_date(REQUEST_TIME)));
    }

    return $target;
  }

  public function targetFinalize($source, $target, $new) {
    // Tnode
    if (!empty($source->tnid)) {
      $tnode = ContentSyncEntity::getTargetBySourceId('node', $source->tnid);
      $target->tnid = $tnode->nid;
    }

    if ($source->uid != 0 && $source->uid != 1) {
      $uid_task = $new 
        ? variable_get('node_sync_uid_task_creation', NODE_SYNC_UID_TASK_ADMIN)
        : variable_get('node_sync_uid_task_editing', NODE_SYNC_UID_TASK_NO_ACTION);
      
      if ($uid_task == NODE_SYNC_UID_TASK_SYNCHRONIZE) {
        $userTarget = ContentSyncEntity::getTargetBySourceId('user', $source->uid);
        $target->uid = $userTarget->uid;
      }
    }

    $target->revision = 0;
  }
}