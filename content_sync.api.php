<?php
/**
 * @file
 *   Exposed Hooks in 7.x:
 */

/**
 * Allows other modules to recognize differences between two entities.
 * @param string $entity_type Entity type
 * @param object $source Source entity
 * @param object $target Target entity
 * @return array List of properties the difference consists of
 */
function hook_entity_diff($entity_type, $source, $target) {
  if ($entity_type == 'node') {
    if ($source->title != $target->title) {
      return array('title');
    }
  }
}

/**
 * Returns a list of entity dependencies
 * <ul>
 *  <li><b>[dependency uuid]</b> =>
 *    <ul>
 *      <li><b>type</b> => [entity type]</li>
 *    </ul>
 *  </li>
 * </ul>
 * @param string $entity_type Entity type
 * @param object $entity Entity
 * @returns array List of dependencies
 */
function hook_entity_clone_dependencies($entity_type, $entity) {
  // No example code
}

/**
 * Allows other modules to define custom clone classes for specific entity types
 * @param type $entity_clone_class Array like:
 * <ul>
 *  <li><b>[entity type]</b> =>
 *    <ul>
 *      <li><b>class</b> => [class (must implement ContentSyncEntityCloneInterface)]</li>
 *    </ul>
 *  </li>
 *  <li><b>[entity type]</b> => ...</li>
 * </ul>
 */
function hook_entity_clone_class_alter(&$entity_clone_class) {
  // No example code
}

/**
 * Allows other modules to do special fixing before the cloned entity is saved
 * Neither the clone nor its dependencies are garanteed to exist when this hook is fired
 * @param string $entity_type Entity type
 * @param object $source Source entity
 * @param object $target Target entity
 * @param object $original_target Original target entity (NULL if it does not exist yet)
 */
function hook_entity_clone_initialize($entity_type, $source, $target, $original_target = NULL) {
  // No example code
}

/**
 * Allows other modules to do special fixing before the cloned entity is saved
 * Either the clone or its dependencies are garanteed to exist when this hook is fired
 * @param string $entity_type Entity type
 * @param object $source Source entity
 * @param object $target Target entity
 * @param boolean $new True if the entity did not exist before initializing
 */
function hook_entity_clone_finalize($entity_type, $source, $target, $new) {
  // No example code
}