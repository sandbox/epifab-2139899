
/**
 * @file
 * Handles with the content synchronization form.
 */

(function($) {
  Drupal.behaviors.contentSync = {
    attach: function(context, settings) {
      if (settings.contentSync != undefined) {
        dependencyUuids = JSON.parse(settings.contentSync.dependencies);
        for (puuid in dependencyUuids) {
          for (i = 0; i < dependencyUuids[puuid].length; i++) {
            $('input[type=checkbox].content-sync-approval.uuid-' + dependencyUuids[puuid][i]).addClass('puuid-' + puuid);
          }
        }

        Drupal.contentSync.reset();

        $('input[type=checkbox].content-sync-approval').each(function(index, element) {
          $(element).click(function() {
            Drupal.contentSync.checkboxTrigger($(element))
          })
        });
        $('input[name="approve_all"]').click(function() {
          Drupal.contentSync.approveAllTrigger()
        });
      }
    }
  }
  
  Drupal.contentSync = {
    checkboxDependencies: function(uuid) {
      if (Drupal.contentSync.data == undefined) {
        Drupal.contentSync.data = {};
      }
      if (Drupal.contentSync.data[uuid] == undefined) {
        Drupal.contentSync.data[uuid] = {
          'requiredBy': {},
          'requiredCount': 0
        }
      }
      return Drupal.contentSync.data[uuid];
    },
    
    reset: function() {
      Drupal.contentSync.data = {};
      
      $('input[type=checkbox].content-sync-approval').each(function(index, element) {
        checkbox = $(element);
        Drupal.contentSync.checkboxTrigger(checkbox);
      });
      
      total = $('input[type=checkbox].content-sync-approval').length;
      selected = $('input[type=checkbox].content-sync-approval:checked').length;
      
      if (selected == 0) {
        $('input[name="approve_all"]').removeAttr('checked');
      }
      else if (selected == total) {
        $('input[name="approve_all"]').attr('checked', 'checked');
      }
    },
    
    checkboxTrigger: function(checkbox) {
      var uuid = checkbox.data('uuid');
      console.log(uuid);
      //var puuid = checkbox.data('puuid');

      var clones = $('input[type=checkbox].content-sync-approval.uuid-' + uuid);

      var input = $('input[name="approval[' + uuid + ']"]');

      //var parents = $('input[type=checkbox].content-sync-approval.uuid-' + puuid);
      var children = $('input[type=checkbox].content-sync-approval.puuid-' + uuid);

      if (checkbox.is(':checked')) {
        clones.attr('checked', 'checked');
        input.val('1');
        children.each(function(i, e) {
          if ($(e).data('action') == 'create') {
            var uuid = $(e).data('puuid');
            var cuuid = $(e).data('uuid');
            if (!Drupal.contentSync.checkboxDependencies(cuuid).requiredBy[uuid]) {
              Drupal.contentSync.checkboxDependencies(cuuid).requiredBy[uuid] = true;
              Drupal.contentSync.checkboxDependencies(cuuid).requiredCount++;

              childClones = $('input[type=checkbox].content-sync-approval.uuid-' + cuuid);
              childClones.attr('disabled', 'disabled');
              childClones.attr('checked', 'checked');
              Drupal.contentSync.checkboxTrigger($(e));
            }
          }
        });
        total = $('input[type=checkbox].content-sync-approval').length;
        selected = $('input[type=checkbox].content-sync-approval:checked').length;
        if (selected == total) {
          $('input[name="approve_all"]').attr('checked', 'checked');
        }
      }
      else {
        clones.removeAttr('checked');
        input.val('0');
        children.each(function(i, e) {
          if ($(e).data('action') == 'create') {
            var uuid = $(e).data('puuid');
            var cuuid = $(e).data('uuid');
            if (Drupal.contentSync.checkboxDependencies(cuuid).requiredBy[uuid]) {
              delete Drupal.contentSync.checkboxDependencies(cuuid).requiredBy[uuid];
              Drupal.contentSync.checkboxDependencies(cuuid).requiredCount--;

              if (Drupal.contentSync.checkboxDependencies(uuid).requiredCount == 0) {
                childClones = $('input[type=checkbox].content-sync-approval.uuid-' + cuuid);
                childClones.removeAttr('disabled', 'disabled');
              }
            }
          }
        });
        $('input[name="approve_all"]').removeAttr('checked');
      }
    },
    
    approveAllTrigger: function() {
      var approveAllCheckbox = $('input[name="approve_all"]');
      if (approveAllCheckbox.is(':checked')) {
        $('input[type=checkbox].content-sync-approval').attr('checked', 'checked');
      }
      else {
        $('input[type=checkbox].content-sync-approval').removeAttr('checked');
        $('input[type=checkbox].content-sync-approval').removeAttr('disabled');
      }
      Drupal.contentSync.reset();
    },
  }
})(jQuery);