This module provides a method to syncrhonize contents (entities) with a master
 Drupal database.

A typical use case is a multisite architecture, where administrators need the
 ability to synchronize subsite contents with a master website.

Out of the box it comes with submodules to handle with standard Drupal entities
 such as nodes, taxonomy terms and files.
The module can be easily extended in order to synchronize different entities or
 to add special fixing for specific entities. As an example, few submodules are
 available to handle with node books, courses and quizzes.

The module provides two ways to synchronize content with the master database.

- Manual synchronization
  It is possible to manually synchronize an existing content by visiting
   "content_sync/synchronize/[ENTITY TYPE]/[ENTITY ID]" or the "syncrhonize" tab
   added to node, term and file pages.

- Using a master registry (requires the additional module "content_sync_master"
   to be correctly installed on the master website)
  The master registry (which is basically a table on the master database)
    actually works like a log, provinding information about created, edited and
    deleted contents on the master website. Then by visiting the administration
    page at "Configuration / Content authoring / Synchronization / Synchronize"
    the administrator is taken through a list of updated contents to synchronize
    and manually approve or refuse the changes for each entity involved.

--

Installation:

 1) Enable content_sync and every other sync module you wish to use.
 2) Visit admin/config/content/content_sync/configure
     then make sure the master database and domain details are correctly set.
 3) Make sure the DBMS user has permission to read from the master database
     configured in (2). In order to grant user permission on MySql 5, please
     refer to http://dev.mysql.com/doc/refman/5.1/en/grant.html.

--

Dependencies:

- Entity API
- UUID
