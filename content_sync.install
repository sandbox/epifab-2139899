<?php

/**
 * @file
 * Module installation file for the content synchronization module.
 */

/**
 * Implements hook_schema()
 */
function content_sync_schema() {
  $schema = array();
  
  $schema['content_sync'] = array(
    'description' => 'Sync list',
    'fields' => array(
      'id' => array(
        'description' => 'Content id',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'entity_uuid' => array(
        'description' => 'Entity UUID',
        'type' => 'varchar',
        'length' => 36,
        'not null' => TRUE
      ),
      'entity_type' => array(
        'description' => 'Entity type',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE
      ),
      'entity_id' => array(
        'description' => 'Entity id',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'entity_bundle' => array(
        'description' => 'Entity bundle',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => ''
      ),
      'entity_label' => array(
        'description' => 'Entity label',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => ''
      ),
      'updated' => array(
        'description' => 'Update timestamp',
        'type' => 'int',
        'not null' => TRUE
      ),
      'deleted' => array(
        'description' => 'Whether the content has been deleted',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0
      ),
      'imported' => array(
        'description' => 'Imported timestamp',
        'type' => 'int',
        'not null' => TRUE
      ),
      'synchronized' => array(
        'description' => 'Sync timestamp',
        'type' => 'int',
        'not null' => FALSE,
        'default' => NULL
      ),
      'approved' => array(
        'description' => 'Synchronization approved',
        'type' => 'int',
        'not null' => FALSE,
        'default' => NULL
      ),
    ),
    'unique keys' => array(
      'entity_id' => array('entity_type', 'entity_id'),
      'entity_uuid' => array('entity_uuid')
    ),
    'primary key' => array('id'),
  );
  
  return $schema;
}

/**
 * Add entity label field to content_sync
 */
function content_sync_update_7100() {
  if (!db_field_exists('content_sync', 'entity_label')) {
    db_add_field('content_sync', 'entity_label', array(
      'description' => 'Entity label',
      'type' => 'varchar',
      'length' => 128,
      'not null' => TRUE,
      'default' => ''
    ));
  }
}

/**
 * Add entity bundle field to content_sync
 */
function content_sync_update_7101() {
  if (!db_field_exists('content_sync', 'entity_bundle')) {
    db_add_field('content_sync', 'entity_bundle', array(
      'description' => 'Entity bundle',
      'type' => 'varchar',
      'length' => 128,
      'not null' => TRUE,
      'default' => ''
    ));
  }
}