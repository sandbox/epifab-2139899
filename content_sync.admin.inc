<?php

/**
 * @file
 * Administration pages callback for the content synchronization module.
 */

/**
 * Menu callback [CONTENT_SYNC_MENU_PREFIX]/config
 */
function content_sync_config_form($form, $form_state) {
  $form = array(
    'main' => array(
      'content_sync_db' => array(
        '#title' => t('Master Database'),
        '#type' => 'textfield',
        '#default_value' => variable_get('content_sync_db'),
        '#required' => TRUE,
      ),
      'content_sync_domain' => array(
        '#title' => t('Master Domain'),
        '#type' => 'textfield',
        '#default_value' => variable_get('content_sync_domain'),
        '#required' => TRUE
      ),
      'content_sync_display_previews' => array(
        '#title' => t('Display content previews'),
        '#type' => 'checkbox',
        '#default_value' => variable_get('content_sync_display_previews', 1),
      ),
      'content_sync_display_uptodate_items' => array(
        '#title' => t('Display up to date items'),
        '#type' => 'checkbox',
        '#default_value' => variable_get('content_sync_display_uptodate_items', 1),
      ),
      'content_sync_uptodate_force_sync' => array(
        '#title' => t('Force up to date items synchronization'),
        '#type' => 'checkbox',
        '#default_value' => variable_get('content_sync_uptodate_force_sync', 0),
      ),
    )
  );
  
  return system_settings_form($form);
}

/**
 * Helper function to get the content_sync_form session
 */
function &content_sync_form_session() {
  if (!isset($_SESSION['content_sync'])) {
    $_SESSION['content_sync'] = array();
  }
  if (empty($_SESSION['content_sync'])) {
    $_SESSION['content_sync']['ignored_items'] = array();
  }
  return $_SESSION['content_sync'];
}

//function content_sync_render_source_page($entity_type, $uuid) {
//  $render = '';
//
//  $source = ContentSyncEntity::getSourceByUuid($entity_type, $uuid);
//
//  if ($source) {
//    $entity_uri = entity_uri($entity_type, $source);
//
//    if (!empty($entity_uri) && isset($entity_uri['path'])) {
//      drupal_goto(variable_get('content_sync_domain') . $entity_uri['path']);
//    }
//    else {
//      drupal_set_message(t('Unable to render this content'), 'warning');
//      $render = '';
//    }
//  }
//  else {
//    drupal_set_message(t('Content not found'), 'warning');
//  }
//
//  return $render;
//}

//function content_sync_render_dest_page($entity_type, $uuid) {
//  $render = '';
//
//  $original = ContentSyncEntity::getOriginalTargetByUuid($entity_type, $uuid);
//
//  if ($original) {
//    $render = entity_view($entity_type, $original);
//    if (!$render) {
//      drupal_set_message(t('Unable to render this content'), 'warning');
//      $render = '';
//    }
//  }
//  else {
//    drupal_set_message(t('Content not found'), 'warning');
//  }
//  
//  return $render;
//}


/**
 * Menu callback [CONTENT_SYNC_MENU_PREFIX]/synchronize
 */
function content_sync_form($form, $form_state) {
  try {
    $sync = ContentSync::getInstance();
    
    $session = &content_sync_form_session();

    $item = NULL;
    foreach ($sync->getRegistry() as $ritem) {
      if (!in_array($ritem->entity_uuid, $session['ignored_items'])) {
        $item = $sync->getItem($ritem->entity_uuid);
        if (!$item->isEvaluated() && !$item->branchIsUpToDate()) {
          break;
        }
        else {
          $item = NULL;
        }
      }
    }

    $form['main'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'id' => 'content-sync-main'
      )
    );

    if (!empty($item)) {
      content_sync_form_main($form['main'], $item);

      $form['main']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Apply'),
        '#submit' => array('content_sync_form_apply_submit'),
      );

      $form['main']['ignore'] = array(
        '#type' => 'submit',
        '#value' => t('Skip for now'),
        '#submit' => array('content_sync_form_ignore_submit'),
        '#ajax' => array(
          'callback' => 'content_sync_form_ajax_callback',
          'wrapper' => 'content-sync-main'
        )
      );
    }
    else {
      $form['main']['uptodate'] = array(
        '#prefix' => '<h3>',
        '#suffix' => '</h3>',
        '#markup' => t('Content up to date')
      );
      $form['main']['reset'] = array(
        '#type' => 'submit',
        '#value' => t('Refresh'),
        '#submit' => array('content_sync_form_reset_submit'),
        '#ajax' => array(
          'callback' => 'content_sync_form_ajax_callback',
          'wrapper' => 'content-sync-main'
        )
      );
    }
  }
  catch (Exception $ex) {
    drupal_set_message(t('An error occurred on updating the content synchronization registry. Please make sure the master database exists, the current SQL user has access to it and the <em>Content Synchronization Master</em> module is installed on the master website.'), 'error');
    drupal_goto(CONTENT_SYNC_MENU_PREFIX . '/configure');
  }
  
  return $form;
}

//function format_bytes($bytes) {
//  if ($bytes > 1024 * 1024) {
//    return round($bytes / (1024 * 1024)) . 'MB';
//  }
//  elseif ($bytes > 1024) {
//    return round($bytes / 1024) . 'KB';
//  }
//  else {
//    return $bytes . 'B';
//  }
//}

function content_sync_form_main(&$main, ContentSyncItem $item) {

  if (variable_get('content_sync_display_previews', TRUE) && !$item->branchIsUpToDate()) {
    foreach ($item->getAllDependencies() as $subitem) {
      if (!$subitem->isUpToDate() && $subitem->getEntityType() == 'node') {
        $source_entity = '';

        $result = drupal_http_request('http://' . variable_get('content_sync_domain') . '/content_sync/' . $subitem->getEntityType() . '/' . $subitem->getEntityUuid());
        if (!empty($result) && !isset($result->error)) {
          $source_entity = drupal_json_decode($result->data);
        }
        else {
          $source_entity = t('Source preview currently not available');
        }

        if (!isset($main['previews'])) {
          $main['previews'] = array(
            '#type' => 'fieldset',
            '#title' => t('Synchronization preview'),
            '#collapsible' => TRUE,
          );
        }

        $main['previews'][$subitem->getEntityUuid()] = array(
          '#type' => 'fieldset',
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
          '#title' => $subitem->getSourceEntity()->title
        );

        $main['previews'][$subitem->getEntityUuid()]['source'] = array(
          '#type' => 'container',
          '#attributes' => array(
            'class' => array('content-sync-item-preview', 'source-preview')
          ),
          'preview' => array(
            '#type' => 'markup',
            '#markup' => $source_entity
          )
        );

        if ($subitem->getTargetEntity()) {
          $main['previews'][$subitem->getEntityUuid()]['target'] = array(
            '#type' => 'container',
            '#attributes' => array(
              'class' => array('content-sync-item-preview', 'target-preview')
            ),
            'preview' => entity_view($subitem->getEntityType(), array($subitem->getTargetEntity()))
          );
        }
      }
    }
  }
  
  $main['approve_all'] = array(
    '#type' => 'checkbox',
    '#default_value' => 1,
    '#title' => t('Approve all')
  );

  $main['approval'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    '#attributes' => array(
      'class' => array('approval-controls-wrapper')
    ),
  );

  $main['entity_type'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );

  $main['action'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );

  $main['item'] = array(
    '#type' => 'value',
    '#value' => $item->getEntityUuid()
  );

  $main['elements'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content branch to synchronize'),
    '#collapsible' => TRUE,
  );

  content_sync_form_item($main, $main['elements'], $item);
  
  $dependency_graph = array();
  if (!empty($item)) {
    foreach ($item->getAllDependencies() as $subitem) {
      $dependency_graph[$subitem->getEntityUuid()] = array();
      foreach ($subitem->getDependencies() as $dependency) {
        if ($subitem->getEntityUuid() != $dependency->getEntityUuid()) {
          $dependency_graph[$subitem->getEntityUuid()][] = $dependency->getEntityUuid();
        }
      }
    }
  }

  $main['#attached'] = array(
    'css' => array(
      drupal_get_path('module', 'content_sync') . '/css/content_sync.css'
    ),
    'js' => array(
      array(
        'data' => array('contentSync' => array('dependencies' => drupal_json_encode($dependency_graph))),
        'type' => 'setting'
      ),
      array(
        'data' => drupal_get_path('module', 'content_sync') . '/js/content_sync.js',
        'type' => 'file'
      )
    )
  );
}

/**
 * Recursively add all checkboxes following the dependency tree
 */
function content_sync_form_item(&$main, &$elements, ContentSyncItem $item, $parent_uuid = NULL, $level = 1) {
  if (isset($main['approval'][$item->getEntityUuid()])) {
    return; // Prevent recursion
  }

  $action = NULL;
  if ($item->isUpToDate()) {
    $action = t('Up to date');
  }
  else {
    switch ($item->getAction()) {
      case CONTENT_SYNC_CREATE:
        $action = t('Create');
        break;
      case CONTENT_SYNC_UPDATE:
        $action = t('Update');
        break;
      case CONTENT_SYNC_DELETE:
        $action = t('Delete');
        break;
    }
  }

  $entity_type = $item->getEntityType();

  if ($item->getAction() != CONTENT_SYNC_DELETE) {
    if ($entity_bundle = entity_extract_bundle($entity_type, $item->getSourceEntity())) {
      $entity_type .= ' ' . $entity_bundle;
    }
  }
  
  if ($item->isUpToDate()) {
    $main['approval'][$item->getEntityUuid()] = array(
      '#type' => 'value',
      '#value' => '-1'
    );
  }
  else {
    $main['approval'][$item->getEntityUuid()] = array(
      '#type' => 'textfield',
      '#default_value' => 1,
      '#attributes' => array(
        'class' => array('item-approval'),
        'data-uuid' => $item->getEntityUuid()
      )
    );
  }
  
  $main['entity_type'][$item->getEntityUuid()] = array(
    '#type' => 'value',
    '#value' => $item->getEntityType(),
  );
  
  $main['action'][$item->getEntityUuid()] = array(
    '#type' => 'value',
    '#value' => $item->getAction(),
  );
  
  if (!$item->isUpToDate() || variable_get('content_sync_display_uptodate_items', TRUE)) {
    $elements[$item->getEntityUuid()] = array(
      '#type' => 'container',
      'approval_' . $item->getEntityUuid() . '_' . $level => array(
        '#type' => 'checkbox',
        '#title' => t('!action !type "<strong>%label</strong>" %uuid', array(
          '!action' => $action, 
          '!type' => $entity_type,
          '%label' => $item->getEntityLabel(),
          '%uuid' => $item->getEntityUuid()
        )),
        '#default_value' => $item->isUpToDate() ? 0 : 1,
        '#disabled' => $item->isUpToDate(),
        '#attributes' => array(
          'class' => array(
            'uuid-' . $item->getEntityUuid(),
            'action-' . $item->getAction(),
            ($item->isUpToDate() ? 'content-sync-approval-uptodate' : 'content-sync-approval')
          ),
          'data-uuid' => $item->getEntityUuid(),
          'data-puuid' => $parent_uuid,
          'data-action' => $item->getAction(),
        ),
        '#prefix' => '<div class="approval-item-wrapper ' . ($item->isUpToDate() ? 'uptodate' : 'outdated') . '">',
        '#suffix' => '</div>'
      ),
      'dependencies' => array(
        '#type' => 'container',
        '#attributes' => array(
          'class' => array(
            'dependencies'
          ),
          'style' => 'padding-left: 15px;'
        )
      ),
    );
  }

  foreach ($item->getDependencies() as $subitem) {
    content_sync_form_item($main, $elements[$item->getEntityUuid()]['dependencies'], $subitem, $item->getEntityUuid(), $level + 1);
  }
}

/**
 * Content sync form ajax callback handler
 */
function content_sync_form_ajax_callback($form, $form_state) {
  return $form['main'];
}

/**
 * Content sync form submit handler (apply button)
 */
function content_sync_form_apply_submit($form, &$form_state) {
  content_sync_sync_entity_form_submit($form, $form_state);
}

/**
 * Content sync form submit handler (skip for now button)
 */
function content_sync_form_ignore_submit($form, &$form_state) {
  $session = &content_sync_form_session();
  if (!in_array($form_state['values']['item'], $session['ignored_items'])) {
    $session['ignored_items'][] = $form_state['values']['item'];
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Content sync form submit handler (reload button)
 */
function content_sync_form_reset_submit($form, &$form_state) {
  $session = &content_sync_form_session();
  $session['ignored_items'] = array();
  $form_state['rebuild'] = TRUE;
}

/**
 * Menu callback for taxonomy/term/%/sync
 */
function content_sync_sync_entity_page($entity_type, $id) {
  if (($entity = ContentSyncEntity::getOriginalTarget($entity_type, $id))) {
    return drupal_get_form('content_sync_sync_entity_form', $entity_type, entity_extract_uuid($entity_type, $entity));
  }
  return MENU_NOT_FOUND;
}

/**
 * Synchronize entity form
 */
function content_sync_sync_entity_form($form, &$form_state, $entity_type, $uuid) {
  
  $item = ContentSyncItem::getItem($entity_type, $uuid);
  
  if ($item) {
    $form['entity_type'] = array(
      '#type' => 'value',
      '#value' => $entity_type
    );
    
    $form['main'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'id' => 'content-sync-main'
      )
    );
    
    content_sync_form_main($form['main'], $item);
    
    $form['main']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Apply'),
    );
  }
  else {
    
  }
  return $form;
}

/**
 * Synchronize entity form submit
 */
function content_sync_sync_entity_form_submit($form, &$form_state) {
  $approved_uuids = array();
  $refused_uuids = array();

  $initialize_operations = array();
  $finalize_operations = array();
  $delete_operations = array();

  foreach ($form_state['values']['approval'] as $uuid => $approved) {
    if ($approved == -1) {
      $approved = variable_get('content_sync_uptodate_force_sync', 0);
    }
    
    $entity_type = $form_state['values']['entity_type'][$uuid];
    $action = $form_state['values']['action'][$uuid];
    
    if ($approved) {
      $approved_uuids[] = $uuid;
      
      switch ($action) {
        case CONTENT_SYNC_CREATE:
        case CONTENT_SYNC_UPDATE:
          $initialize_operations[] = array('content_sync__batch_apply__task_target_initialize', array($entity_type, $uuid));
          $finalize_operations[] = array('content_sync__batch_apply__task_target_finalize', array($entity_type, $uuid, (int)$action == CONTENT_SYNC_CREATE));
          break;
        case CONTENT_SYNC_DELETE:
          $delete_operations[] = array('content_sync__batch_apply__task_delete',            array($entity_type, $uuid));
          break;
      }
    }
    
    else {
      $refused_uuids[] = $uuid;
    }
  }
  
  $operations = array_merge($initialize_operations, $finalize_operations, $delete_operations);
  $operations[] = array('content_sync__batch_apply__task_update_registry', array($approved_uuids, $refused_uuids));
  
  batch_set(array(
   'operations' => $operations,
   'finished' => 'content_sync__batch_apply__finish',
   'file' => drupal_get_path('module', 'content_sync') . '/content_sync.admin.inc',
   'title' => t('Content synchronization'),
   'init_message' => t('Initializing...'),
   'progress_message' => t('Processed @current out of @total.'),
   'error_message' => t('An error occurred. Please try later'),
  ));
}

/**
 * Batch API callback. Target initialize
 * @param string $entity_type Entity type
 * @param string $uuid Uuid
 * @param array $context Batch context
 */
function content_sync__batch_apply__task_target_initialize($entity_type, $uuid, &$context) {
  try {
    $target = ContentSyncEntity::targetInitialize($entity_type, $uuid);
    $context['message'] = t('@type %label initialized', array(
      '@type' => $entity_type,
      '%label' => entity_label($entity_type, $target)
    ));
  }
  catch (Exception $ex) {
    watchdog('content_sync', 'An error occurred while initializing @type %label: <pre>@details</pre><pre>@trace</pre>', array(
      '@type' => $entity_type,
      '%label' => $uuid,
      '@details' => $ex->getMessage(),
      '@trace' => $ex->getTraceAsString()
    ), WATCHDOG_ERROR);
    
    die(json_encode(array('status' => FALSE, 'data' => t('An error occurred while initializing @type %label: <pre>@details</pre>', array(
      '@type' => $entity_type,
      '%label' => $uuid,
      '@details' => $ex->getMessage(),
    )))));
  }
}

/**
 * Batch API callback. Target finalize
 * @param string $entity_type Entity type
 * @param string $uuid Uuid
 * @param bool $new True if the entity did not exist before initializing
 * @param array $context Batch context
 */
function content_sync__batch_apply__task_target_finalize($entity_type, $uuid, $new, &$context) {
  try {
    $target = ContentSyncEntity::targetFinalize($entity_type, $uuid, $new);
    $context['message'] = t('@type %label finalized', array(
      '@type' => $entity_type,
      '%label' => entity_label($entity_type, $target)
    ));
  }
  catch (Exception $ex) {
    watchdog('content_sync', 'An error occurred while finalizing @type %label: <pre>@details</pre><pre>@trace</pre>', array(
      '@type' => $entity_type,
      '%label' => $uuid,
      '@details' => $ex->getMessage(),
      '@trace' => $ex->getTraceAsString()
    ), WATCHDOG_ERROR);
    
    die(json_encode(array('status' => FALSE, 'data' => t('An error occurred while finalizing @type %label: <pre>@details</pre>', array(
      '@type' => $entity_type,
      '%label' => $uuid,
      '@details' => $ex->getMessage(),
    )))));
  }
}

/**
 * Batch API callback. Target deletion
 * @param string $entity_type Entity type
 * @param string $uuid Uuid
 * @param array $context Batch context
 */
function content_sync__batch_apply__task_delete($entity_type, $uuid, &$context) {
  try {
    ContentSyncEntity::deleteOriginalTarget($entity_type, $uuid);
    $context['message'] = t('@type %uuid deleted', array(
      '@type' => $entity_type,
      '%uuid' => $uuid
    ));
  }
  catch (Exception $ex) {
    watchdog('content_sync', 'An error occurred while deleting @type %label: <pre>@details</pre><pre>@trace</pre>', array(
      '@type' => $entity_type,
      '%label' => $uuid,
      '@details' => $ex->getMessage(),
      '@trace' => $ex->getTraceAsString()
    ), WATCHDOG_ERROR);
    
    die(json_encode(array('status' => FALSE, 'data' => t('An error occurred while deleting @type %label: <pre>@details</pre>', array(
      '@type' => $entity_type,
      '%label' => $uuid,
      '@details' => $ex->getMessage(),
    )))));
  }
}

/**
 * Batch API callback. Update synchronization registry
 * @param array $uuids List of uuid => TRUE|FALSE, where "uuid" stands for the entity uuid while approval is a boolean
 * @param string $uuid Uuid
 * @param array $context Batch context
 */
function content_sync__batch_apply__task_update_registry($approved_uuids, $refused_uuids, &$context) {
  if (!empty($approved_uuids)) {
    db_update('content_sync')
      ->fields(array('synchronized' => REQUEST_TIME, 'approved' => 1))
      ->condition('entity_uuid', $approved_uuids, 'IN')
      ->execute();
  }

  if (!empty($refused_uuids)) {
    db_update('content_sync')
      ->fields(array('synchronized' => REQUEST_TIME, 'approved' => 0))
      ->condition('entity_uuid', $refused_uuids, 'IN')
      ->execute();
  }

  $context['message'] = t('Content sync registry updated');
}

/**
 * Batch API 'finished' callback.
 * @param bool $success
 * @param array $results
 * @param array $operations
 */
function content_sync__batch_apply__finish($success, $results, $operations) {
  if ($success) {
    // Here we do something meaningful with the results.
    drupal_set_message(t('Content synchronization completed'));
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => implode(', ', $error_operation[1]),
    )), 'error');
  }
}
