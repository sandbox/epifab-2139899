<?php

/**
 * @file
 * Functions and classes to interact with the for the content synchronization module
 */

/**
 * Returns the list of fields for the given entity type and bundle
 * @return array Associative array ([field name] => [field type], [field 2 name] => [field 2 type], ...)
 */
function entity_field_types($entity_type, $bundle = NULL) {
  static $fields = NULL;

  if (!isset($fields)) {
    $fields = &drupal_static(__FUNCTION__, array());
  }

  if (!isset($fields[$entity_type]) || !isset($fields[$entity_type][$bundle])) {
    $q = "SELECT DISTINCT i.field_name, f.type"
       . " FROM {field_config_instance} i"
       . " INNER JOIN {field_config} f ON f.id = i.field_id"
       . " WHERE i.entity_type = :entity_type AND f.deleted = 0 AND i.deleted = 0"
       . (empty($bundle) ? "" : " AND bundle = :bundle");

    $p = array(':entity_type' => $entity_type) + (empty($bundle) ? array() : array(':bundle' => $bundle));

    $fields[$entity_type][$bundle] = db_query($q, $p)->fetchAllKeyed(); // Uses name as key and type as value
  }

  return $fields[$entity_type][$bundle];
}

function entity_extract_id($entity_type, $entity) {
  list($id) = entity_extract_ids($entity_type, $entity);
  return $id;
}

function entity_extract_vid($entity_type, $entity) {
  list(, $vid) = entity_extract_ids($entity_type, $entity);
  return $vid;
}

function entity_extract_bundle($entity_type, $entity) {
  list(, , $bundle) = entity_extract_ids($entity_type, $entity);
  return $bundle;
}

function entity_extract_uuid($entity_type, $entity, $revision = FALSE) {
  $info = entity_get_info($entity_type);

  $uuid_key = NULL;
  if (!$revision) {
    $uuid_key = $info['entity keys']['uuid'];
  }
  elseif (isset($info['revision table'])) {
    $uuid_key = $info['entity keys']['revision uuid'];
  }
  
  if (empty($uuid_key) || !isset($entity->{$uuid_key})) {
    return FALSE;
  }
  else {
    return $entity->{$uuid_key};
  }
}

class ContentSyncEntity {
  
  private static $entities = array();
  private static $entitiesByUuid = array();
  
  private static $targetEntities = array();
  
  private static $classes = array();
  
  private static function getEntity($key, $entityType, $entityId) {
    if (!isset(self::$entities[$key])) {
      self::$entities[$key] = &drupal_static("content_sync_entities:{$key}");
    }
    if (!isset(self::$entities[$key][$entityType]) || !isset(self::$entities[$key][$entityType][$entityId])) {
      try {
        switch ($key) {
          case 'source':
            $dbk = ContentSync::connectToMaster();
            break;
          case 'original':
            $dbk = ContentSync::connectToSlave();
            break;
          default:
            return NULL;
        }
        
        $entity = self::load($entityType, $entityId);
        
        if (!empty($entity)) {
          $entity->content_sync_entity_key = $key;
          
          self::$entities[$key][$entityType][$entityId] = $entity;

          $uuid = entity_extract_uuid($entityType, $entity);
          if (!empty($uuid)) {
            self::$entitiesByUuid[$key][$uuid] = $entity;
          }
        }
        else {
          self::$entities[$key][$entityType][$entityId] = NULL;
        }

        db_set_active($dbk);
      }
      catch (Exception $ex) {
        ContentSync::connectToSlave();
        throw $ex;
      }
    }
    
    return self::$entities[$key][$entityType][$entityId];
  }
  
  private static function getEntityByUuid($key, $entityType, $uuid) {
    if (!isset(self::$entitiesByUuid[$key])) {
      self::$entitiesByUuid[$key] = &drupal_static("content_sync_entities_by_uuid:{$key}");
    }
    if (!isset(self::$entitiesByUuid[$key][$uuid])) {
      try {
        switch ($key) {
          case 'source':
            $dbk = ContentSync::connectToMaster();
            break;
          case 'original':
            $dbk = ContentSync::connectToSlave();
            break;
          default:
            return NULL;
        }
        
        $ids = entity_get_id_by_uuid($entityType, array($uuid));
        
        $entity = (!empty($ids) && isset($ids[$uuid]))
           ? self::load($entityType, $ids[$uuid])
           : NULL;
        
        if (!empty($entity)) {
          $entity->content_sync_entity_key = $key;
          
          self::$entitiesByUuid[$key][$uuid] = $entity;
          self::$entities[$key][$entityType][$ids[$uuid]] = $entity;
          $entity->content_sync_entity_key = $key;
        }
        else {
          self::$entitiesByUuid[$key][$uuid] = NULL;
        }
        
        db_set_active($dbk);
      }
      catch (Exception $ex) {
        ContentSync::connectToSlave();
        throw $ex;
      }
    }
    
    return self::$entitiesByUuid[$key][$uuid];
  }
  
  public static function getSource($entityType, $entityId) {
    return self::getEntity('source', $entityType, $entityId);
  }
  
  public static function getSourceByUuid($entityType, $uuid) {
    return self::getEntityByUuid('source', $entityType, $uuid);
  }
  
  public static function getOriginalTarget($entityType, $entityId) {
    return self::getEntity('original', $entityType, $entityId);
  }
  
  public static function getOriginalTargetByUuid($entityType, $uuid) {
    return self::getEntityByUuid('original', $entityType, $uuid);
  }
  
  public static function getTargetByUuid($entityType, $uuid, $strict = FALSE) {
    if (isset(self::$targetEntities[$uuid])) {
      return self::$targetEntities[$uuid];
    }
    elseif (!$strict) {
      return self::getOriginalTargetByUuid($entityType, $uuid);
    }
    else {
      return NULL;
    }
  }
  
  /**
   * Return the target entity by the source entity id
   * @param string $entityType Entity type
   * @param int $sourceId Source entity id
   * @return object Target entity (NULL if not found)
   */
  public static function getTargetBySourceId($entityType, $sourceId) {
    $source = self::getSource($entityType, $sourceId);
    if (!empty($source)) {
      $uuid = entity_extract_uuid($entityType, $source);
      if ($uuid) {
        return self::getTargetByUuid($entityType, $uuid);
      }
    }
    return NULL;
  }
  
  /**
   * Return the original target entity by the source entity id
   * @param string $entityType Entity type
   * @param int $sourceId Source entity id
   * @return object Original target entity (NULL if not found)
   */
  public static function getOriginalTargetBySourceId($entityType, $sourceId) {
    $source = self::getSource($entityType, $sourceId);
    if (!empty($source)) {
      $uuid = entity_extract_uuid($entityType, $source);
      if ($uuid) {
        return self::getOriginalTargetByUuid($entityType, $uuid);
      }
    }
    return NULL;
  }
  
  /**
   * Initialize a clone from a given entity
   * @param string $entityType Entity type
   * @param string $uuid Entity uuid
   * @return object Target entity
   * @throws Exception
   */
  public static function targetInitialize($entityType, $uuid) {
    watchdog('content_sync', 'Initializing @type %uuid', array('@type' => $entityType, '%uuid' => $uuid), WATCHDOG_DEBUG);
    
    $source = self::getSourceByUuid($entityType, $uuid);
    $originalTarget = self::getOriginalTargetByUuid($entityType, $uuid);
    
    if (!$source) {
      watchdog('content_sync', 'Source entity not found for %uuid', array('%uuid' => $uuid), WATCHDOG_ERROR);
      throw new Exception('Unknown entity UUID');
    }
    
    // Initialize the clone
    $target = self::entityCloneClass($entityType)->targetInitialize($source, $originalTarget);
    
    // Allows other modules to do special fixing here
    module_invoke_all('entity_clone_initialize', $entityType, $source, $target, $originalTarget);

    // Save the clone
    self::entityCloneClass($entityType)->save($target);

    self::$targetEntities[$uuid] = $target;
    $target->content_sync_entity_key = 'target';
    
    return $target;
  }
  
  /**
   * Finalize and store a clone
   * @param string $entityType Entity type
   * @param string $uuid Entity uuid
   * @param boolean $new True if the entity did not exist before initializing
   * @return object Target entity
   */
  public static function targetFinalize($entityType, $uuid, $new) {
    watchdog('content_sync', 'Finalizing @type %uuid', array('@type' => $entityType, '%uuid' => $uuid), WATCHDOG_DEBUG);
    
    $source = self::getSourceByUuid($entityType, $uuid);
    $target = self::getTargetByUuid($entityType, $uuid);
    
    if (!$target) {
      watchdog('content_sync', 'Target entity not found for %uuid', array('%uuid' => $uuid), WATCHDOG_ERROR);
      throw new Exception('Unknown entity UUID');
    }
    
    // Finalize the clone
    self::entityCloneClass($entityType)->targetFinalize($source, $target, $new);
    
    // Allows other modules to do special fixing here
    module_invoke_all('entity_clone_finalize', $entityType, $source, $target, $new);

    // Save the clone
    self::entityCloneClass($entityType)->save($target);
    
    self::$targetEntities[$uuid] = $target;
    $target->content_sync_entity_key = 'target';
    
    return $target;
  }
  
  /**
   * Returns the entity clone class for the given entity type
   * @param string $entityType Entity type
   * @return ContentSyncEntityCloneInterface Object to handle with entities for the given entity type
   */
  private static function entityCloneClass($entityType) {
    $classInfo = &drupal_static('entity_clone_class', NULL);

    if ($classInfo === NULL) {
      $classInfo = array();
      drupal_alter('entity_clone_class', $classInfo);
    }
    
    if (isset(self::$classes[$entityType])) {
      return self::$classes[$entityType];
    }
    elseif (isset($classInfo[$entityType])) {
      if (!empty($classInfo[$entityType]['class'])) {
        
        $class = $classInfo[$entityType]['class'];
        $interfaces = class_exists($class) ? class_implements($class) : FALSE;
        
        if ($interfaces && in_array('ContentSyncEntityCloneInterface', $interfaces)) {
          self::$classes[$entityType] = new $classInfo[$entityType]['class']($entityType);
        }
      }
      
      if (empty(self::$classes[$entityType])) {
        watchdog('content_sync', 'Entity clone class not correctly defined for !type entities. ContentSyncEntityCloneDefault will be used instead', array('!type' => $entityType), WATCHDOG_ERROR);
        self::$classes[$entityType] = new ContentSyncEntityCloneDefault($entityType);
      }
    }
    else {
      self::$classes[$entityType] = new ContentSyncEntityCloneDefault($entityType);
    }
    
    return self::$classes[$entityType];
  }
  
  /**
   * Load additional dependencies specific for the entity type
   * @param string $entityType Entity type
   * @param object $source Entity
   * @return array List of dependency info as 'entity' => [Entity object], 'type' => [Entity type], 'id' => [Entity id], 'uuid' => [Entity uuid]
   * @throws Exception
   */
  public static function sourceDependencies($sourceType, $source) {
    $allDependencies = self::entityCloneClass($sourceType)->sourceDependencies($source);
    
    $sourceId = entity_extract_id($sourceType, $source);
    
    if (empty($allDependencies)) {
      $allDependencies = array();
    }
    elseif (!is_array($allDependencies)) {
      watchdog('content_sync', '%type entity %id: Invalid dependency info', array('%type' => $sourceType, '%id' => $sourceId), WATCHDOG_ERROR);
      $allDependencies = array();
    }
    
    foreach (module_implements('entity_clone_dependencies') as $module) {
      $dependencies = module_invoke($module, 'entity_clone_dependencies', $sourceType, $source);
      if (empty($dependencies)) {
        $dependencies = array();
      }
      elseif (!is_array($dependencies)) {
        watchdog('content_sync', 'Invalid dependency info returned by !module module', array('!module' => $module), WATCHDOG_ERROR);
        $dependencies = array();
      }
      $allDependencies = array_merge($allDependencies, $dependencies);
    }
    
    $allDependenciesByUuid = array();
    
    foreach ($allDependencies as $dependencyInfo) {
      if (!is_array($dependencyInfo)) {
        watchdog('content_sync', '%type entity %id: Invalid dependency info <pre>@info</pre>', array(
          '%type' => $sourceType,
          '%id' => $sourceId,
          '@info' => print_r($dependencyInfo, TRUE)
        ), WATCHDOG_ERROR);
        continue;
      }
      elseif (!isset($dependencyInfo['type'])) {
        watchdog('content_sync', '%type entity %id: Invalid dependency info (unknown entity type)', array(
          '%type' => $sourceType,
          '%id' => $sourceId,
          '@info' => print_r($dependencyInfo, TRUE)
        ), WATCHDOG_ERROR);
        continue;
      }
      elseif (!isset($dependencyInfo['id'])) {
        watchdog('content_sync', '%type entity %id: Invalid dependency info (unknown entity id)', array(
          '%type' => $sourceType,
          '%id' => $sourceId,
          '@info' => print_r($dependencyInfo, TRUE)
        ), WATCHDOG_ERROR);
        continue;
      }
      else {
        $dType = $dependencyInfo['type'];
        $dId = $dependencyInfo['id'];
        $dEntity = ContentSyncEntity::getSource($dependencyInfo['type'], $dependencyInfo['id']);
        if (empty($dEntity)) {
          watchdog('content_sync', '%type entity %id: Invalid dependency info "%desc" (%dtype entity %did not found)', array(
            '%type' => $sourceType,
            '%id' => $sourceId,
            '%desc' => isset($dependencyInfo['description']) ? $dependencyInfo['description'] : '',
            '%did' => $dId,
            '%dtype' => $dType
          ), WATCHDOG_ERROR);
          continue;
        }
        else {
          $dUuid = entity_extract_uuid($dType, $dEntity);
          if (empty($dUuid)) {
            watchdog('content_sync', 'Unable to retrieve the UUID for %type entity %id', array(
              '%type' => $dType, 
              '%id' => $dId
            ), WATCHDOG_ERROR);
            continue;
          }
          elseif (!isset($allDependencies[$dUuid])) {
            $allDependenciesByUuid[$dUuid] = array(
              'type' => $dType,
              'id' => $dId,
              'entity' => $dEntity,
              'uuid' => $dUuid,
            );
          }
        }
      }
    }
    
    return $allDependenciesByUuid;
  }
  
  /**
   * Load an entity using the clone load method or entity_load_single (@see ContentSyncEntity::info)
   * Ensure to flush the entity cache.
   * @param string $entityType Entity type
   * @param int $entityId Entity id
   * @return object Entity
   * @throws Exception
   */
  private static function load($entityType, $entityId) {
    $entity = $obj = self::entityCloneClass($entityType)->load($entityId);
    // Avoid entity cache
    entity_get_controller($entityType)->resetCache();
    return $entity;
  }
  
  /**
   * Delete an entity using the clone delete method (@see ContentSyncEntity::info)
   * @param string $entityType Entity type
   * @param string $uuid Entity uuid
   * @throws Exception
   */
  public static function deleteOriginalTarget($entityType, $uuid) {
    $originalTarget = self::getOriginalTargetByUuid($entityType, $uuid);
    
    if (!empty($originalTarget)) {
      $entityId = entity_extract_id($entityType, $originalTarget);
      self::entityCloneClass($entityType)->delete($entityId);
      return $originalTarget;
    }
    return NULL;
  }
}

interface ContentSyncEntityCloneInterface {
  /**
   * Load the entity
   * @param int $entityId Entity id
   * @return object Entity loaded
   */
  public function load($entityId);
  
  /**
   * Save the entity
   * @param object $entity Entity
   */
  public function save($entity);
  
  /**
   * Delete the entity
   * @param int $entityId Entity id
   */
  public function delete($entityId);
  
  /**
   * Initialize a new clone for the given source entity
   * @param object $sourceEntity The source entity
   * @param object $originalTargetEntity The original target entity (default to NULL on creation)
   * @return object The target entity
   */
  public function targetInitialize($sourceEntity, $originalTargetEntity = NULL);

  /**
   * Returns a list of dependencies
   * @param object $sourceEntity Source entity
   * @return array List of dependencies
   */
  public function sourceDependencies($sourceEntity);
  
  /**
   * Finalize an entity clone
   * @param object $sourceEntity The source entity
   * @param object $targetEntity The target entity
   */
  public function targetFinalize($sourceEntity, $targetEntity, $new);
}


class ContentSyncEntityCloneDefault implements ContentSyncEntityCloneInterface {
  private $entityType;
  
  public function __construct($entityType) {
    if (empty($entityType)) {
      throw new Exception('Entity type not specified');
    }
    $this->entityType = $entityType;
  }
  
  public function load($entityId) {
    return entity_load_single($this->entityType, $entityId);
  }

  public function save($entity) {
    entity_save($this->entityType, $entity);
  }

  public function delete($entityId) {
    entity_delete($this->entityType, $entityId);
  }

  public function sourceDependencies($entity) {
    return array();
  }

  public function targetInitialize($sourceEntity, $originalTargetEntity = NULL) {
    // @todo: 
    //  Further investigate on a more efficient way to clone the original object
    //  ensuring that all variable references gets broken.
    //  
    // Simply doing $target = clone($sourceEntity) seems not to be working properly.
    // 
    // A inexplicable weird issue I've been running into is as follows:
    // 
    // $target = clone($sourceEntity);
    // ... (doing other stuff)
    // $target->field_tags = array();
    // 
    // At this point, $sourceEntity->field_tags is an empty array!!
    //$target = unserialize(serialize($sourceEntity));
    $target = clone $sourceEntity;

    $entityInfo = entity_get_info($this->entityType);
    $idKey = $entityInfo['entity keys']['id'];
    $revisionKey = $entityInfo['entity keys']['revision'];

    if (!empty($originalTargetEntity)) {
      $target->{$idKey} = $originalTargetEntity->{$idKey};
      if ($revisionKey) {
        $target->{$revisionKey} = $originalTargetEntity->{$revisionKey};
      }
    }
    else {
      unset($target->{$idKey});
      if ($revisionKey) {
        unset($target->{$revisionKey});
      }
    }

    return $target;
  }

  public function targetFinalize($sourceEntity, $targetEntity, $new) {
    
  }
}
