<?php

/**
 * @file
 * Core classes for the content synchronization module
 */

class ContentSyncItem {
  private static $instances = array();
  
  private $action;
  private $entityType;
  private $entityUuid;
  private $entityLabel;
  private $sourceEntityId;
  private $sourceEntity;
  private $targetEntityId;
  private $targetEntity;
  
  private $evaluated = FALSE;
  private $approved = TRUE;
  private $applied = FALSE;
  
  private $entityUpToDate = NULL;
  
  private $dependencies = array();
  private $allDependencies = NULL;
  
  public static function getInstances() {
    return self::$instances;
  }
  
  public static function itemExists($entityUuid) {
    return isset(self::$instances[$entityUuid]);
  }
  
  /**
   * Get a synchronizable entity
   * @param string $entityType Entity type
   * @param string $entityUuid Entity UUID
   * @return ContentSyncItem Get a content sync item
   */
  public static function getItem($entityType, $entityUuid) {
    if (!isset(self::$instances[$entityUuid])) {
      self::$instances[$entityUuid] = new self($entityType, $entityUuid);
      self::$instances[$entityUuid]->loadDependencies();
    }
    return self::$instances[$entityUuid];
  }
  
  private function __construct($entityType, $entityUuid) {
    $this->entityType = $entityType;
    $this->entityUuid = $entityUuid;

    $this->sourceEntity = ContentSyncEntity::getSourceByUuid($entityType, $entityUuid);
    $this->sourceEntityId = empty($this->sourceEntity) ? NULL : entity_extract_id($entityType, $this->sourceEntity);

    $this->targetEntity = ContentSyncEntity::getOriginalTargetByUuid($entityType, $entityUuid);
    $this->targetEntityId = empty($this->targetEntity) ? NULL : entity_extract_id($entityType, $this->targetEntity);

    $this->entityLabel = empty($this->sourceEntity) ? NULL : entity_label($entityType, $this->sourceEntity);
    if (empty($this->entityLabel)) {
      $this->entityLabel = empty($this->targetEntity) ? NULL : entity_label($entityType, $this->targetEntity);
    }

    if (empty($this->sourceEntityId)) {
      $this->action = CONTENT_SYNC_DELETE;
    }
    else {
      if (empty($this->targetEntityId)) {
        $this->action = CONTENT_SYNC_CREATE;
      }
      else {
        //@todo Work out if the master entity di
        $this->action = CONTENT_SYNC_UPDATE;
      }
    }
  }
  
  private function loadDependencies() {
    if (!empty($this->sourceEntity)) {
      $dependenciesInfo = ContentSyncEntity::sourceDependencies($this->entityType, $this->sourceEntity);

      foreach ($dependenciesInfo as $dependencyInfo) {
        if (!isset($this->dependencies[$dependencyInfo['uuid']])) {
          $this->dependencies[$dependencyInfo['uuid']] = self::getItem($dependencyInfo['type'], $dependencyInfo['uuid']);
        }
      }
    }
  }
  
//  private function addDependency(ContentSyncItem $dependency) {
//    if ($dependency->entityUuid == $this->entityUuid) {
//      return;
//    }
//    if ($dependency->isUpToDate()) {
//      foreach ($dependency->getDependencies() as $d) {
//        $this->addDependency($d);
//      }
//    }
//    else {
//      $this->dependencies[$dependency->entityUuid] = $dependency;
//    }
//  }
  
  /**
   * List of uuid => item 
   * Every item that can be reached by recursively following every dependency
   * @return ContentSyncItem[] Dependency items
   */
  public function getAllDependencies() {
    if (is_null($this->allDependencies)) {
      $this->allDependencies = array();
      $this->_getAllDependencies($this->allDependencies);
    }
    return $this->allDependencies;
  }
  
  private function _getAllDependencies(&$dependencies) {
    $dependencies[$this->entityUuid] = $this;
    foreach ($this->dependencies as $dependency) {
      if (!isset($dependencies[$dependency->entityUuid])) {
        $dependency->_getAllDependencies($dependencies);
      }
    }
  }
  
  /**
   * Approve content synchronization
   * @param array $uuidsApproval Associative array [uuid] => [TRUE|FALSE] of uuids to approve or to refuse.
   * @param boolean $approveByDefault Whether to automatically approve or not items that are not in the $uuids array
   * @throws Exception
   */
  public function apply($uuidsApproval = NULL, $approveByDefault = FALSE) {
    $t = db_transaction();
          
    try {
      field_info_cache_clear();
      
      if (!is_null($uuidsApproval)) {
        // Custom approval
        foreach ($this->getAllDependencies() as $subitem) {
          if (!$subitem->applied) {
            // If an item has already been applied, it can't be ignored  anymore
            $subitem->approved = isset($uuidsApproval[$subitem->getEntityUuid()])
              ? $uuidsApproval[$subitem->getEntityUuid()]
              : $approveByDefault;
          }
        }
      }

      $targetEntitiesInfo = array();

      $approvedUuids = array();
      $refusedUuids = array();

      
      foreach ($this->getAllDependencies() as $subitem) {
        if ($subitem->isApplied()) {
          // Flag up to date items as approved
          $approvedUuids[] = $subitem->entityUuid;
        }
        
        else {
          if ($subitem->approved) {

            $approvedUuids[] = $subitem->entityUuid;

            switch ($subitem->action) {
              case CONTENT_SYNC_UPDATE:
              case CONTENT_SYNC_CREATE:
                // Ensure that new dependencies will be approved
                foreach ($subitem->getDependencies() as $dependency) {
                  if (!$dependency->approved && $dependency->action == CONTENT_SYNC_CREATE) {
                    watchdog('content_sync', '%dtype entity %duuid creation must be approved in order to !action %type entity %uuid', array(
                      '%dtype' => $dependency->entityType,
                      '%duuid' => $dependency->entityUuid,
                      '!action' => $subitem->action,
                      '%type' => $subitem->entityType,
                      '%uuid' => $subitem->entityUuid
                    ), WATCHDOG_ERROR);
                    throw new Exception(t('Unexisting dependency'));
                  }
                }

//                watchdog('content_sync', '%type entity clone initializing: %uuid', array(
//                  '%type' => $subitem->entityType,
//                  '%uuid' => $subitem->entityUuid
//                ), WATCHDOG_DEBUG);
                
                $originalTarget = $subitem->targetEntity;
                
                $subitem->targetEntity = ContentSyncEntity::targetInitialize($subitem->entityType, $subitem->entityUuid);
                $subitem->targetEntityId = entity_extract_id($subitem->entityType, $subitem->targetEntity);
                
                $targetEntitiesInfo[] = array('type' => $subitem->entityType, 'uuid' => $subitem->entityUuid, 'new' => empty($originalTarget));
                break;

              case CONTENT_SYNC_DELETE:
                ContentSyncEntity::deleteOriginalTarget($subitem->entityType, $subitem->entityUuid);
                $subitem->targetEntity = NULL;
                $subitem->targetEntityId = NULL;
                break;
            }
            
            // Changes have been applied
            $subitem->applied = $subitem->approved;
          }

          else {
            $refusedUuids[] = $subitem->entityUuid;
          }
        }
        
        $subitem->evaluated = TRUE;
      }

      foreach ($targetEntitiesInfo as $targetEntityInfo) {
//        watchdog('content_sync', '%type entity clone finalizing: %uuid', array(
//          '%type' => $subitem->entityType,
//          '%uuid' => $subitem->entityUuid
//        ), WATCHDOG_DEBUG);

        ContentSyncEntity::targetFinalize($targetEntityInfo['type'], $targetEntityInfo['uuid'], $targetEntityInfo['new']);
      }
      
      if (!empty($approvedUuids)) {
        db_update('content_sync')
          ->fields(array('synchronized' => REQUEST_TIME, 'approved' => 1))
          ->condition('entity_uuid', $approvedUuids, 'IN')
          ->execute();
      }
      
      if (!empty($refusedUuids)) {
        db_update('content_sync')
          ->fields(array('synchronized' => REQUEST_TIME, 'approved' => 0))
          ->condition('entity_uuid', $refusedUuids, 'IN')
          ->execute();
      }
      
      try {
        // Flush the cache after each synchronization
        cache_clear_all();
      }
      catch (Exception $ex) { 
        // Ignore errors while flushing the cache at this stage
      }

      return array($approvedUuids, $refusedUuids);
    }
    catch (Exception $ex) {
      $t->rollback();
      throw $ex;
    }
  }
  
  /**
   * Update action
   * <ul>
   *  <li>CONTENT_SYNC_CREATE</li>
   *  <li>CONTENT_SYNC_UPDATE</li>
   *  <li>CONTENT_SYNC_DELETE</li>
   * </ul>
   * @return string Action
   */
  public function getAction() {
    return $this->action;
  }
  
  /**
   * @return string Entity type
   */
  public function getEntityType() {
    return $this->entityType;
  }
  
  /**
   * @return string Entity universal unique identifier
   */
  public function getEntityUuid() {
    return $this->entityUuid;
  }
  
  /**
   * @return string Entity label
   */
  public function getEntityLabel() {
    return $this->entityLabel;
  }
  
  /**
   * @return string Source entity label
   */
  public function getSourceEntityLabel() {
    return empty($this->sourceEntity) ? '' : entity_label($this->entityType, $this->sourceEntity);
  }
  
  /**
   * @return string Target entity label
   */
  public function getTargetEntityLabel() {
    return empty($this->targetEntity) ? '' : entity_label($this->entityType, $this->targetEntity);
  }
  
  /**
   * @return int Source entity id (or NULL if it does not exist)
   */
  public function getSourceEntityId() {
    return $this->sourceEntityId;
  }
  
  /**
   * @return object Source entity (or NULL if it does not exist)
   */
  public function getSourceEntity() {
    return $this->sourceEntity;
  }
  
  /**
   * @return int Target entity id (or NULL if it does not exist)
   */
  public function getTargetEntityId() {
    return $this->targetEntityId;
  }
  
  /**
   * @return object Target entity (or NULL if it does not exist)
   */
  public function getTargetEntity() {
    return $this->targetEntity;
  }

  /**
   * @return ContentSyncItem[] First level dependencies
   */
  public function getDependencies() {
    return $this->dependencies;
  }
  
  /**
   * @return boolean TRUE if the item has been evaluated
   */
  public function isEvaluated() {
    return $this->evaluated;
  }
  
  /**
   * @return boolean Whether or not to approve the item changes
   */
  public function isApproved() {
    return $this->approved;
  }
  
  /**
   * @return boolean Whether or not the changes have been applied
   */
  public function isApplied() {
    return $this->applied;
  }
  
  /**
   * @return boolean TRUE if the item is up to date or has already been evaluated
   */
  public function isUpToDate() {
    return $this->applied 
      || ($this->action == CONTENT_SYNC_DELETE && empty($this->targetEntity))
      || ($this->action == CONTENT_SYNC_UPDATE && $this->entityIsUpToDate());
  }
  
  /**
   * @return boolean TRUE if the entire branch is up to date
   */
  public function branchIsUpToDate() {
    foreach ($this->getAllDependencies() as $item) {
      if (!$item->isUpToDate()) {
        return FALSE;
      }
    }
    return TRUE;
  }
  
  private function entityIsUpToDate() {
    if ($this->entityUpToDate === NULL) {
      $this->entityUpToDate = TRUE;
      foreach (module_implements('entity_diff') as $module) {
        $diff = module_invoke($module, 'entity_diff', $this->entityType, $this->sourceEntity, $this->targetEntity);
        if (!empty($diff)) {
          $this->entityUpToDate = FALSE;
          break;
        }
      }
    }
    return $this->entityUpToDate;
  }
}

class ContentSync {
  private static $instance;
  
  private $registry;
  
  /**
   * Returns a ContentSync instance of FALSE if a problem occurred
   *  on initializing the insntace.
   * @return ContentSync
   */
  public static function getInstance() {
    if (is_null(self::$instance)) {
      self::$instance = new self();
    }
    return self::$instance;
  }
  
  
  private function __construct() {
    self::importContentSyncMaster();
    
    try {
      $this->registry = db_query("SELECT * FROM {content_sync} WHERE synchronized IS NULL OR synchronized < updated")->fetchAllAssoc('entity_uuid');
    }
    catch (Exception $ex) {
      watchdog('content_sync', 'Unable to retrieve a list of content to synchronize. Please check whether the current sql user has properly set read permission and the sync_content_master module is installed on the master database.', array('%name' => variable_get('content_sync_db')), WATCHDOG_ERROR);
      
      throw $ex;
    }
  }
  
  /**
   * Get the content sync registry
   * @return array Content sync registry
   */
  public function getRegistry() {
    return $this->registry;
  }
  
  /**
   * Returns a ContentSyncItem or NULL if it does not exist
   * @param string $uuid Item UUID
   * @return ContentSyncItem Item
   */
  public function getItem($uuid) {
    if (isset($this->registry[$uuid])) {
      $item = ContentSyncItem::getItem($this->registry[$uuid]->entity_type, $uuid);
      if (!empty($item) && $item->branchIsUpToDate()) {
        // Update the content sync registry to flag this item as approved
        db_update('content_sync')
          ->fields(array('synchronized' => REQUEST_TIME, 'approved' => 1))
          ->condition('entity_uuid', $item->getEntityUuid())
          ->execute();
      }
      return $item;
    }
  }
  
  /**
   * Connect to the master database
   */
  public static function connectToMaster() {
    static $first = TRUE;
    
    if ($first) {
      $dbName = variable_get('content_sync_db', NULL);
      
      if (empty($dbName)) {
        watchdog('content_sync', 'Unable to connect to the master database. The database name is undefined.', array(), WATCHDOG_ERROR);
        throw new Exception('Undefined master database name');
      }
      
      $masterConnection = Database::getConnectionInfo('default');
      $masterConnection['default']['database'] = $dbName;
      
      // Initializing master connection details
      Database::addConnectionInfo('content_sync_master', 'default', $masterConnection['default']);
      $first = FALSE;
    }
    
    try {
      return db_set_active('content_sync_master');
    }
    catch (Exception $ex) {
      watchdog('content_sync', 'Unable to connect to the master database %name. Please check whether the database exists and the current sql user has properly set read permission.', array('%name' => variable_get('content_sync_db')), WATCHDOG_ERROR);
      throw $ex;
    }
  }
  
  /**
   * Connect to the slave database
   */
  public static function connectToSlave() {
    return db_set_active();
  }
  
  /**
   * Import the content sync master registry
   * @throws Exception
   */
  private static function importContentSyncMaster() {
    $lastSync = db_query("SELECT MAX(imported) FROM {content_sync}")->fetchField();
    
    try {
      self::connectToMaster();

      $res = (!$lastSync)
        ? db_query("SELECT * FROM {content_sync_master}")
        : db_query("SELECT * FROM {content_sync_master} WHERE updated > :time", array(':time' => $lastSync));

      $contentSyncMaster = $res->fetchAllAssoc('entity_uuid');

      self::connectToSlave();

      foreach ($contentSyncMaster as $content_sync_item) {
        db_merge('content_sync')
          ->fields(array(
            'entity_uuid' => $content_sync_item->entity_uuid,
            'entity_type' => $content_sync_item->entity_type,
            'entity_id' => $content_sync_item->entity_id,
            'entity_label' => $content_sync_item->entity_label,
            'entity_bundle' => $content_sync_item->entity_bundle,
            'updated' => $content_sync_item->updated,
            'deleted' => $content_sync_item->deleted,
            'imported' => REQUEST_TIME,
            'synchronized' => NULL,
            'approved' => NULL
          ))->condition('entity_uuid', $content_sync_item->entity_uuid)
          ->execute();
      }
    }
    catch (Exception $ex) {
      self::connectToSlave();
      watchdog('content_sync', 'Unable to import content sync registry from the master database', array(), WATCHDOG_ERROR);
      throw $ex;
    }
  }
}